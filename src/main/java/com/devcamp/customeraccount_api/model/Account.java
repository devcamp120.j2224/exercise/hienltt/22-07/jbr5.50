package com.devcamp.customeraccount_api.model;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [balance=" + balance + ", customer=" + customer + ", id=" + id + "]";
    }

    public String getCustomerName(){
        return this.customer.getName();
    }

    public double deposit(double amount){
        return this.balance = amount += this.balance;
    }
    
    public double withdraw(double amount){
        if(amount <= this.balance){
            this.balance -= amount;
        } else {
            System.out.println("Account exceeds balance");
        }
        return this.balance;
    }
}
