package com.devcamp.customeraccount_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccount_api.model.Customer;
@Service
public class CustomerService {
    Customer customer1 = new Customer(01, "Nguyen Van A", 8);
    Customer customer2 = new Customer(02, "Nguyen Van B", 9);
    Customer customer3 = new Customer(03, "Nguyen Van C", 10);

    public Customer getCustomer1() {
        return customer1;
    }

    public Customer getCustomer2() {
        return customer2;
    }

    public Customer getCustomer3() {
        return customer3;
    }

    public ArrayList<Customer> getAllCustomer(){
        ArrayList<Customer> lCustomers = new ArrayList<>();
        lCustomers.add(customer1);
        lCustomers.add(customer2);
        lCustomers.add(customer3);
        return lCustomers;
    }
}
