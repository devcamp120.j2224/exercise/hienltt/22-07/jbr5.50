package com.devcamp.customeraccount_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccount_api.model.Account;
@Service
public class AccountService {
    CustomerService customer = new CustomerService();
    Account account1 = new Account(001, customer.getCustomer1(), 500000);
    Account account2 = new Account(002, customer.getCustomer2(), 1000000);
    Account account3 = new Account(003, customer.getCustomer3(), 1500000);

    public ArrayList<Account> getAllAccount(){
        ArrayList<Account> lAccount = new ArrayList<>();
        lAccount.add(account1);
        lAccount.add(account2);
        lAccount.add(account3);
        return lAccount;
    }
}
