package com.devcamp.customeraccount_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccount_api.model.Account;
import com.devcamp.customeraccount_api.service.AccountService;

@RestController
public class AccountController {
    @Autowired
    AccountService accountService;

    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        ArrayList<Account> listAccount = accountService.getAllAccount();

        return listAccount;
    }
}
